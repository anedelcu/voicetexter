package com.alexnedelcu.voicetexter.states;

import android.content.Context;
import android.media.AudioManager;

public class Audio {
	AudioManager am ;
	public Audio (Context c) {
		am = (AudioManager) c.getSystemService(c.AUDIO_SERVICE);
	}
	
	public boolean isHeadsetOn() {
		 return am.isWiredHeadsetOn();
	}
	public boolean isBluetoothHeadsetOn() {
		return am.isBluetoothA2dpOn();
	}
	public boolean isRingerNormal() {
		return am.getRingerMode() == am.RINGER_MODE_NORMAL;
	}
	public boolean isRingerVibrate() {
		return am.getRingerMode() == am.RINGER_MODE_VIBRATE;
	}
	public boolean isRingerSilent() {
		return am.getRingerMode() == am.RINGER_MODE_SILENT;
	}
}
