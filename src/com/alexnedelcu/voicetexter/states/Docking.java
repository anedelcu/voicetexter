package com.alexnedelcu.voicetexter.states;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class Docking {
	private boolean docked;
	public IntentFilter ifilter;
	private Context c;
	
	public boolean isDocked() {
        Intent dockStatus = c.registerReceiver(null, ifilter);
        
        int dockState = (dockStatus == null ?
	        Intent.EXTRA_DOCK_STATE_UNDOCKED :
	        dockStatus.getIntExtra(Intent.EXTRA_DOCK_STATE, -1));
//	    boolean isDocked = dockState != Intent.EXTRA_DOCK_STATE_UNDOCKED;
	    boolean isCar = dockState == Intent.EXTRA_DOCK_STATE_CAR;
	    
		return isCar;
	}
	
	void watchDocking(Context c) {
		this.c = c;
		ifilter = new IntentFilter(Intent.ACTION_DOCK_EVENT);
	}
}
