package com.alexnedelcu.voicetexter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.alexnedelcu.voicetexter.IO.IO;
import com.alexnedelcu.voicetexter.util.Command;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class Locality {
	LocationManager locationManager;
	private static Locality instance;
	ArrayList<Location> locations = new ArrayList<Location>();
	private Command actionWhenActivate=null, actionWhenDeactivate=null;
	
	private static boolean isDriving=false;;
	private static final int TWO_MINUTES = 1000 * 60 * 2;
	private static final double MIN_DRIVING_SPEED = 3; // meters per second (4 = 14.4 km/h)
	private static final int MIN_NUM_NON_DRIVING_USER_SIGNALS = 3; // decides after how many signals the non-driving mode is activated
	private static final int MIN_NUM_DRIVING_USER_SIGNALS = 3; // decides after how many signals the driving mode is activated

	public static Locality getInstance(Activity activity) {
		if (instance == null)
			instance = new Locality(activity);
		return instance;
	}
	
	private Locality (Activity activity) {
		// Acquire a reference to the system Location Manager
		locationManager = (LocationManager) activity.getSystemService(activity.getApplicationContext().LOCATION_SERVICE);

		// Define a listener that responds to location updates
		LocationListener locationListener = new LocationListener() {
		    public void onLocationChanged(Location location) {
		      // Called when a new location is found by the network location provider.
		    	
		    	// Record the new location
		    	
		    	if (locations.size() == 0 || isBetterLocation(location, locations.get(locations.size()-1))) {
		    		locations.add(location);

		    		Date currDate = new Date();
		    		Date locationTime = new Date(location.getTime());
		    		SimpleDateFormat date_parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");   
		    		
		    		appendLog(isDriving + ","+
		    				location.getAccuracy() + "," +
		    				location.getAltitude() + "," +
		    				location.getBearing() + "," +
		    				location.getLatitude() + "," +
		    				location.getLongitude() + "," +
		    				location.getProvider() + "," +
		    				location.hasSpeed() + "," +
		    				location.getSpeed() + "," +
		    				location.getTime() + "," +
		    				date_parser.format(locationTime) + "," +
		    				currDate.getTime() + "," +
		    				date_parser.format(currDate)
		    				);
		    	}
		    	
		    	// Remove old locations and decide if the user is driving or not
		    	while (locations.get(locations.size()-1).getTime() - locations.get(0).getTime() > TWO_MINUTES) {
		    		long elapsedTime = (locations.get(locations.size()-1).getTime() - locations.get(0).getTime()) / 1000;
		    		float distance = locations.get(locations.size()-1).distanceTo(locations.get(0));
		    		float speed = distance/elapsedTime;

		    		// TODO : a better way to update this variable
		    		if (speed >= MIN_DRIVING_SPEED) 
		    			signalUserDriving();
		    		else
		    			signalUserNotDriving(location);
		    		
		    		locations.remove(0);
		    	}
		    	

		    	System.out.println("User driving : "+isDriving);
		    	
		    }

		    public void onStatusChanged(String provider, int status, Bundle extras) {}

		    public void onProviderEnabled(String provider) {}

		    public void onProviderDisabled(String provider) {}

		  };

		// Register the listener with the Location Manager to receive location updates
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
//		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 0, 0, locationListener);
	}

	private int drivingSignals=0;
	private void signalUserDriving() {
		drivingSignals++;
		if (drivingSignals >= 2 && !isDriving) {
			isDriving = true;
			IO.getInstance().output("Driving mode is active.");
			if (actionWhenActivate != null)
				actionWhenActivate.execute(null);
		}
		
		// reset non driving user vars
		resetSignalsUserNotDriving();
	}
	private void resetSignalsUserDriving() {
		drivingSignals = 0;
	}
	
	private long restSignalTime = 0;
	private int restSignals = 0;
	private void signalUserNotDriving(Location loc) {
		if(isDriving) {
			restSignals ++;
			if (restSignalTime == 0) restSignalTime = loc.getTime();
			if (loc.getTime() - restSignalTime > TWO_MINUTES && restSignals >= MIN_NUM_NON_DRIVING_USER_SIGNALS) {
				isDriving=false;
				IO.getInstance().output("Driving mode is turned off.");
				if (actionWhenDeactivate != null)
					actionWhenDeactivate.execute(null);
			}
		}
		// reset driving user vars
		resetSignalsUserDriving();
	}
	

	private void resetSignalsUserNotDriving() {
		restSignalTime = 0;
		restSignals = 0;
	}
	
	/**
	 * Informs the client if the user is likely to be driving
	 * @return
	 */
	public boolean isUserDriving() {
		return isDriving;
	}
	

	/** Determines whether one Location reading is better than the current Location fix
	  * @param location  The new Location that you want to evaluate
	  * @param currentBestLocation  The current Location fix, to which you want to compare the new one
	  */
	protected boolean isBetterLocation(Location location, Location currentBestLocation) {
	    if (currentBestLocation == null) {
	        // A new location is always better than no location
	        return true;
	    }
	
	    // Check whether the new location fix is newer or older
	    long timeDelta = location.getTime() - currentBestLocation.getTime();
	    boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
	    boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
	    boolean isNewer = timeDelta > 0;
	
	    // If it's been more than two minutes since the current location, use the new location
	    // because the user has likely moved
	    if (isSignificantlyNewer) {
	        return true;
	    // If the new location is more than two minutes older, it must be worse
	    } else if (isSignificantlyOlder) {
	        return false;
	    }
	
	    // Check whether the new location fix is more or less accurate
	    int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
	    boolean isLessAccurate = accuracyDelta > 0;
	    boolean isMoreAccurate = accuracyDelta < 0;
	    boolean isSignificantlyLessAccurate = accuracyDelta > 200;
	
	    // Check if the old and new location are from the same provider
	    boolean isFromSameProvider = isSameProvider(location.getProvider(),
	            currentBestLocation.getProvider());
	
	    // Determine location quality using a combination of timeliness and accuracy
	    if (isMoreAccurate) {
	        return true;
	    } else if (isNewer && !isLessAccurate) {
	        return true;
	    } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
	        return true;
	    }
	    return false;
	}
	
	/** Checks whether two providers are the same */
	private boolean isSameProvider(String provider1, String provider2) {
	    if (provider1 == null) {
	      return provider2 == null;
	    }
	    return provider1.equals(provider2);
	}
	
	public void appendLog(String text)
	{       
	   File logFile = new File("/storage/sdcard0/locations.8.9.13.txt");
	   if (!logFile.exists())
	   {
	      try
	      {
	         logFile.createNewFile();
	      } 
	      catch (IOException e)
	      {
	         // TODO Auto-generated catch block
	         e.printStackTrace();
	      }
	   }
	   try
	   {
	      //BufferedWriter for performance, true to set append to file flag
	      BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true)); 
	      buf.append(text);
	      buf.newLine();
	      buf.close();
	   }
	   catch (IOException e)
	   {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
	   }
	}
	
	public Location getLastLocation () {
		if (locations.size() == 0) return null;
		return locations.get(locations.size()-1);
	}
	

	public void setActionOnDrivingModeActivation(Command command) {
		actionWhenActivate = command;
	}

	public void setActionOnDrivingModeDeactivation(Command command) {
		actionWhenDeactivate = command;
	}
}
