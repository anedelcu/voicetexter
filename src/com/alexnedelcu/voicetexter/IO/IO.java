package com.alexnedelcu.voicetexter.IO;

import java.util.ArrayList;

import com.alexnedelcu.voicetexter.util.Command;

import android.content.Context;
import android.util.Log;

public class IO {
	private TextToVoice ttv;
	private VoiceToText vtt;
	private static IO instance;
	
	public IO (Context c) {
		ttv = new TextToVoice(c);
		vtt = new VoiceToText(c);
		instance = this;
	}
	
	public static IO getInstance() {
		return instance;
	}
	
	public void output (String text) {
		while (vtt.isListening())
			try {
				Thread.currentThread().sleep(300);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		ttv.speakOut(text);
	}
	
	public void input (String prompt, int noResults, Command action) {
		ttv.speakOut(prompt);
		
		while (ttv.isSpeaking())
			try {
				Thread.currentThread().sleep(300);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		vtt.voiceInput(noResults, action);
	}
	
	public void getYesNoAnswer(final String prompt, final Command actionYes, final Command actionNo) {
		final ArrayList yesAnswers = new ArrayList<String>();

		yesAnswers.add("yes");
		yesAnswers.add("yea");
		yesAnswers.add("yeah");
		yesAnswers.add("of course");
		yesAnswers.add("definitely");

		final ArrayList noAnswers = new ArrayList<String>();
		noAnswers.add("no");
		noAnswers.add("nope");
		
		input(prompt, 3, new Command() {

			@Override
			public void execute(Object params) {
				ArrayList<String> input = (ArrayList<String>) params;
				boolean askAgain = true;
				for (int i=0; i<input.size(); i++) {
					if (noAnswers.contains(input.get(i))) {
						askAgain = false;
						actionNo.execute(null);
						Log.i("read", "no");
						break;
					}
					else if (yesAnswers.contains(input.get(i))) {
						askAgain = false;
						actionYes.execute(null);
						Log.i("read", "yes");
						break;
					}
				}
				
				if (askAgain) getYesNoAnswer(prompt, actionYes, actionNo);
				
			}
			
		});

	}
	
	public void inputMessage(final String prompt, final Command action) {
		input(prompt, 1, new Command() {

			@Override
			public void execute(Object params) {
				ArrayList<String> input = (ArrayList<String>) params;
				action.execute(input.get(0));
			}
		
		});
	}
}
