package com.alexnedelcu.voicetexter.IO;

import java.util.Locale;
import java.util.PriorityQueue;
import java.util.Queue;

import android.annotation.SuppressLint;
import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.util.Log;

public class TextToVoice implements TextToSpeech.OnInitListener {
	private static TextToSpeech tts;
	
//	boolean ready = false;
	
	@SuppressLint("NewApi")
	public TextToVoice (Context c) {
		synchronized(this) {
			Log.e("TTS", "Initilization Started!");
			tts = new TextToSpeech(c, this);
			Log.e("TTS", "Initilization FINISHED!");
		}
	}
	
	public synchronized void speakOut(String text) {
		Log.e("TTS", "SPEAKING " + text);
		tts.speak(text, tts.QUEUE_ADD, null);
	}

	@Override
	public void onInit(int status) {
		if (status == TextToSpeech.SUCCESS) {
       	 
            int result = tts.setLanguage(Locale.US);
 
            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
//                speakOut("What's up?");
            }
 
        } else {
            Log.e("TTS", "Initilization Failed!");
        }
	}

	public boolean isSpeaking() {
		return tts.isSpeaking();
	}
	
	protected void onDestroy() {
	    tts.stop();
	    tts.shutdown();
	}
}
