package com.alexnedelcu.voicetexter.IO;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;

import com.alexnedelcu.voicetexter.util.Command;

public class VoiceToText {
	SpeechRecognizer sr;
	public boolean listening = false;
	
	public VoiceToText(Context c) {
		sr = SpeechRecognizer.createSpeechRecognizer(c);
	}
	
	public void voiceInput(int noResults, Command action) {
		sr.setRecognitionListener(new VoiceToTextListener(this, action));
	    
	    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
	    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
//	    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,Locale.);
	    intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,"com.alexnedelcu.voicetexter");
	    intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS,2000);
	    intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_MINIMUM_LENGTH_MILLIS,5000);
	    intent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS,1500);
	    intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, noResults); 
	    sr.startListening(intent);
	}

	public boolean isListening() {
		// TODO Auto-generated method stub
		return listening;
	}
	
    
}
