package com.alexnedelcu.voicetexter.IO;

import java.util.ArrayList;

import com.alexnedelcu.voicetexter.util.Command;

import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;
import android.util.Log;

public class VoiceToTextListener implements RecognitionListener {
	VoiceToText vtt;
	Command command;
	
	public VoiceToTextListener (VoiceToText pVtt, Command c) {
		vtt = pVtt;
		command = c;
	}
	
	@Override
	public void onBeginningOfSpeech() {
		// TODO Auto-generated method stub
		vtt.listening = true;
		Log.i("STT", "Message started");
		
	}

	@Override
	public void onBufferReceived(byte[] buffer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEndOfSpeech() {
		// TODO Auto-generated method stub
		Log.i("STT", "Message finished");
		vtt.listening = false;
	}

	@Override
	public void onError(int error) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEvent(int eventType, Bundle params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPartialResults(Bundle partialResults) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onReadyForSpeech(Bundle params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onResults(Bundle results) {
		// TODO Auto-generated method stub

        String str = new String();
        Log.d("VTT", "onResults " + results);
        ArrayList data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
        for (int i = 0; i < data.size(); i++)
        {
                  Log.d("VTT", "result " + data.get(i));
                  str += data.get(i);
        }
        
        command.execute(data);
	}

	@Override
	public void onRmsChanged(float rmsdB) {
		// TODO Auto-generated method stub
		
	}

}
