package com.alexnedelcu.voicetexter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferenceInterpreter {
	private SharedPreferences settings;
	private Context context;
	
	public PreferenceInterpreter(Context context) {
		this.context = context;
	}
	
	/**
	 * Check if the user wants the application to auto-activate when driving
	 * 
	 * @return
	 */
	public boolean isDriveModeAllowed() {
		settings = PreferenceManager.getDefaultSharedPreferences(context);
		return settings.getBoolean("active_driving_checkbox", true);
	}
	
	/**
	 * Check if the user wants the application to auto-activate when the phone is docked in a car or using headphones
	 * 
	 * @return
	 */
	public boolean isDockedModeAllowed() {
		settings = PreferenceManager.getDefaultSharedPreferences(context);
		return settings.getBoolean("active_audio_docked", true);
	}
	
	/**
	 * Check if the user wants the application to auto-activate when the user is in a specific location
	 * 
	 * @return
	 */
	public boolean isSpecificLocationsAllowed() {
		settings = PreferenceManager.getDefaultSharedPreferences(context);
		return settings.getBoolean("active_specific_locations", true);
	}
}
