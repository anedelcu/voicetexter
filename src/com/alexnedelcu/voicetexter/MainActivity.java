package com.alexnedelcu.voicetexter;

import java.util.ArrayList;

import com.alexnedelcu.voicetexter.IO.TextToVoice;
import com.alexnedelcu.voicetexter.IO.VoiceToTextListener;
import com.alexnedelcu.voicetexter.map.ui.ActiveLocationsActivity;
import com.alexnedelcu.voicetexter.util.Command;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnHoverListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

@SuppressLint("NewApi")
public class MainActivity extends Activity {
	private UiLifecycleHelper uiHelper;
	private ImageView btnStartStop;
	private ImageView btnSettings;
	private ImageView btnLocations;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
            @Override
            public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
                Log.e("Activity", String.format("Error: %s", error.toString()));
            }

            @Override
            public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
                Log.i("Activity", "Success!");
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        uiHelper.onResume();
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        uiHelper = new UiLifecycleHelper(this, new Session.StatusCallback() {
            @Override
            public void call(Session session, SessionState state, Exception exception) {

            }}); 
        uiHelper.onCreate(savedInstanceState);
        
        SMSManager manager = SMSManager.getInstance(this);
        
        final MainActivity a = this;
        manager.setActionOnDrivingModeActivation (new Command() {

			@Override
			public void execute(Object params) {
				a.switchButtonStartToPause();
				System.out.println("ACtiVE DRIVING MODE: BUtTON CHANGE");
			}
        	
        });
        

        manager.setActionOnDrivingModeDeactivation (new Command() {

			@Override
			public void execute(Object params) {
				a.switchButtonStartToStart();
				System.out.println("Deactivate DRIVING MODE: BUtTON CHANGE BACK");
			}
        	
        });

        // find the buttons from the layout
        btnStartStop = (ImageView) findViewById(R.id.imgStartStop);
        btnLocations = (ImageView) findViewById(R.id.imgLocations);
        btnSettings = (ImageView) findViewById(R.id.imgSettings);
        
        
        // make these behave nicely
        setButtonBehaviorOnView(btnStartStop, new Command() {
        	
        	
			@Override
			public void execute(Object params) {
				
			}
        	
        });
        setButtonBehaviorOnView(btnSettings, new Command() {

        	// setting on click action that opens the map when the Settings button is clicked
			@Override
			public void execute(Object params) {
				openSettings((View) params);
			}
        });
        
        setButtonBehaviorOnView(btnLocations, new Command() {
        	
        	// setting on click action that opens the map when the Locations button is clicked
			@Override
			public void execute(Object params) {
				openLocations((View) params);
			}
        	
        });
        
        
        



        
        // setting on click action that opens the map when the Start/Stop button is clicked
        btnSettings.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
			}
		});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        
        return true;
    }

    public void openSettings(View v) {
    	Intent i = new Intent(this, UserSettingsActivity.class);
    	startActivity(i);
    }

    public void openLocations(View v) {
    	Intent i = new Intent(this, ActiveLocationsActivity.class);
    	startActivity(i);
    }
    
	public void setButtonBehaviorOnView(final View view, final Command c) {
    	
    	view.setOnTouchListener(new OnTouchListener() {
			

			@Override
			public boolean onTouch(View v, MotionEvent event) {

	            switch (event.getAction() & MotionEvent.ACTION_MASK) {

	                case MotionEvent.ACTION_DOWN:
	                case MotionEvent.ACTION_POINTER_DOWN:

	                    //=====Write down your Finger Pressed code here
	                	view.setTranslationX(1);
	                	view.setTranslationY(1);

	                    return true;

	                case MotionEvent.ACTION_UP:
	                case MotionEvent.ACTION_POINTER_UP:

	                	c.execute(v);
	                	
	                    //=====Write down you code Finger Released code here
	                	view.setTranslationX(-1);
	                	view.setTranslationY(-1);
	                	

	                    return true;            
	                }   
				return false;
			}
		});
    }
    

	public void switchButtonStartToPause () {
		btnStartStop.setImageResource(R.drawable.vt_ic_stop);
	}

	public void switchButtonStartToStart () {
		btnStartStop.setImageResource(R.drawable.vt_ic_start);
	}
}
