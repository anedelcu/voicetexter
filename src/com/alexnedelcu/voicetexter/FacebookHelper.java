package com.alexnedelcu.voicetexter;

import java.util.ArrayList;

import android.app.Activity;

import com.facebook.widget.FacebookDialog;

public class FacebookHelper {
	public FacebookHelper(Activity activity) {
		// TODO Auto-generated constructor stub
		// facebook example

		 ArrayList<String> friends = new ArrayList<String>();
		 friends.add("Irina Nedelcu");

		// Check if the Facebook app is installed and we can present the share dialog
		FacebookDialog.MessageDialogBuilder builder = new FacebookDialog.MessageDialogBuilder(activity)
		    .setLink("https://developers.facebook.com/docs/android/share/")
		    .setName("NAME").setFriends(friends);

		// If the Facebook app is installed and we can present the share dialog
		if (builder.canPresent()) {
			// Present message dialog
			FacebookDialog dialog = builder.build();
			if (dialog.canPresentMessageDialog(activity.getApplicationContext(), FacebookDialog.MessageDialogFeature.MESSAGE_DIALOG)) {
				System.out.println("canPresentMessageDialog is true");
			} else {System.out.println("canPresentMessageDialog is false");}
			dialog.present();
		}  else {
		  // Disable button or other UI for Message Dialog 
		}
	}
}
