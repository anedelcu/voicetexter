package com.alexnedelcu.voicetexter.datatypes;

import android.telephony.SmsManager;


public class Message {
	private String from;
	private String fromContact;
	private String bodyReceived;
	private String bodyReplied;
	
	
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getBodyReplied() {
		return bodyReplied;
	}
	public void setBodyReplied(String body) {
		this.bodyReplied = body;
	}
	
	public void send() {

		SmsManager smsManager = SmsManager.getDefault();
		smsManager.sendTextMessage(this.from, null, this.bodyReplied, null, null);
		
	}
	public String getBodyReceived() {
		return bodyReceived;
	}
	public void setBodyReceived(String bodyReceived) {
		this.bodyReceived = bodyReceived;
	}
}
