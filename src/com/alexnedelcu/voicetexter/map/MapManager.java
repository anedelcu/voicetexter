package com.alexnedelcu.voicetexter.map;

import java.util.ArrayList;
import java.util.Collections;

import android.location.Location;

import com.alexnedelcu.voicetexter.Locality;
import com.alexnedelcu.voicetexter.map.ui.VecinityMarker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public class MapManager {
	
	private GoogleMap map;
	private Location currentLocation;
	private VecinityMarker currentMarker;
	private ArrayList<VecinityMarker> locations = new ArrayList<VecinityMarker>();
	
	public MapManager(GoogleMap pMap) {
		map = pMap;
	}

	/**
	 * Move the view to the current location
	 * @param locality
	 */
	public void zoomOnCurrentLocation () {
		
		LatLng point = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
    	
    	// focus the camera on the current location
		map.moveCamera(CameraUpdateFactory.zoomTo(16));
		map.animateCamera(CameraUpdateFactory.newLatLng(point));
        
        // add the current location
        VecinityMarker currLocMarker = new VecinityMarker(map, point, currentLocation.getAccuracy());
        currLocMarker.addIconMarker(map);
        
        currLocMarker.addRadiusArea();
	}
	
	/**
	 * Set the current location
	 * @param locality
	 */
	public void setCurrentLocation (Locality locality) {
		currentLocation = locality.getLastLocation();
	}
	
	/**
	 * Adds a Temporary Marker on the map without saving it
	 * @param point
	 */
	public void addTemporaryMarker (LatLng point) {
		currentMarker = new VecinityMarker(map, point, 20);
		currentMarker.addOnMap();
		currentMarker.setTitle("[New location]");
		currentMarker.setRadius(currentLocation.getAccuracy());
		currentMarker.addRadiusArea();
		currentMarker.showInfoWindow();
	}
	
	/**
	 * Finds the VecinityMarker based on a Marker
	 * @param m
	 * @return
	 */
	public VecinityMarker getVecinityMarkerByMarker(Marker m) {
		for (int i=0; i<locations.size(); i++)
			if (locations.get(i).getMarker().getPosition().equals(m.getPosition()))
				return locations.get(i);
		
		return null;
	}
	
	/**
	 * Removes the current temporary marker
	 * TODO: May be replaced by removeTemporaryMarker
	 */
	public void cleanCurrentTemporaryUnsavedMarker () {
		if (!locations.contains(currentMarker) && currentMarker != null) {
			currentMarker.remove();
			currentMarker = null;
		}
	}
	
	/**
	 * Gets the current marker
	 * @return
	 */
	public VecinityMarker getCurrentMarker () {
		return currentMarker;
	}
	
	/**
	 * Save the current marker (when done editing)
	 */
	public void saveTemporaryMarker() {
		if (!locations.contains(currentMarker)) {
			locations.add(currentMarker);
		}
	}
	
	/**
	 * Set the radius of the current marker
	 * @param radius
	 */
	public void setTemporaryMarkerRadius (double radius) {
		currentMarker.setRadius(radius);
	}
	
	/**
	 * Redraw the temporary marker radius
	 */
	public void redrawTemporaryMarkerRadius () {
		currentMarker.removeRadiusArea();
		currentMarker.addRadiusArea();
	}
	
	/**
	 * Set the Title of the current marker
	 * @param title
	 */
	public void setTemporaryMarkerTitle (String title) {
		currentMarker.setTitle(title);
	}
	
	/**
	 * Remove the current marker
	 */
	public void removeCurrentMarker() {
		if (currentMarker != null) {
			currentMarker.remove();
			locations.remove(currentMarker);
			currentMarker = null;
		}
	}
	
	/**
	 * Display the info windows of the current marker
	 */
	public void showTemporaryMarkerInfoWindow () {
		currentMarker.showInfoWindow();
	}
	
	
	/**
	 * Change the current marker
	 * @param vm
	 */
	public void setTemporaryMarker(VecinityMarker vm) {
		currentMarker = vm;
	}

	/**
	 * Get all the markers
	 */
	public ArrayList<VecinityMarker> getAllSavedMarkers() {
		return locations;
	}

	/**
	 * Set all the markers
	 */
	public void setAllSavedMarkers(ArrayList<VecinityMarker> markers) {
		locations = markers;
		for (int i=0; i<locations.size(); i++) {
			locations.get(i).addOnMap();
			
		}
	}
	
	/**
	 * Remove all the saved markers on the map
	 */
	public void reset() {
		for (int i=0; i<locations.size(); i++) {
			locations.get(i).remove();
		}

		locations.clear();
		
	}
}
