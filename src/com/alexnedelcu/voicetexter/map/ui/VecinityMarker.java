package com.alexnedelcu.voicetexter.map.ui;

import java.util.ArrayList;
import java.util.Map;

import android.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class VecinityMarker {
	private LatLng latLng;
	private double radius = 0;
	private Color circleLine;
	private Color circleFill;
	private double circleOpacity;
	private Marker marker;
	Circle iconMarker;
	private GoogleMap map;
	private Circle circle;
	
	
	public Marker getMarker() {
		return marker;
	}

	public VecinityMarker(GoogleMap m, LatLng ll, double r) {
		this.map = m;
		this.latLng = ll;
		this.radius = r;
	}
	
	public void addOnMap() {
		this.marker = map.addMarker(new MarkerOptions()
        				.position(this.latLng));
	}

//	public VecinityMarker(GoogleMap map, Marker marker) {
//		this.map = map;
//		this.latLng = marker.getPosition();
//		this.marker = marker;
//		this.radius = 0;
//	}

	public void addRadiusArea() {
		this.circle = map.addCircle(new CircleOptions()
			.center(this.latLng)
			.radius(this.radius)
			.strokeColor(0x902982FA)
			.fillColor(0x502982FA)
			.strokeWidth(3));
	}
	
	public void removeRadiusArea() {
		if (this.circle != null) {
			this.circle.remove();
			this.circle = null;
		}
	}

//	public void addMarker(String title) {
//		this.marker = map.addMarker(new MarkerOptions()
//	        .position(this.latLng)
//	        .title(title));
//	}
	
	public void addIconMarker(GoogleMap m) {
		this.iconMarker = map.addCircle(new CircleOptions()
	        .center(this.latLng)
			.strokeColor(0x202982FA)
			.fillColor(0x992982FA)
	        .radius(this.radius/8));
	}
	
	public void showInfoWindow() {
		marker.showInfoWindow();
	}
	
	public void remove () {
		marker.remove();
		removeRadiusArea();
		System.out.println("Marker removed");
	}
	
	public String getTitle() {
		return marker.getTitle();
	}

	public LatLng getPosition() {
		return marker.getPosition();
	}
	public void setTitle (String title) {
		marker.setTitle(title);
	}

	public void setRadius(double n) {
		this.radius = n;
	}
	public double getRadius() {
		return this.radius;
	}
	public double getLatitude () {
		return marker.getPosition().latitude;
	}
	public double getLongitude () {
		return marker.getPosition().longitude;
	}
}
