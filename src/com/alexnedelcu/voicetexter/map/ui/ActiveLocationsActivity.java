package com.alexnedelcu.voicetexter.map.ui;

import java.util.ArrayList;
import java.util.List;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.alexnedelcu.voicetexter.Locality;
import com.alexnedelcu.voicetexter.R;
import com.alexnedelcu.voicetexter.map.MapManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ActiveLocationsActivity extends Activity {
	private GoogleMap mMap;
//	Location currentLocation;
	
	private MapManager mapManager;
	private LocationsStorage ls;
	
	// UI elements
    RelativeLayout laySaveMarker;
    Button btnSaveMarker, btnDeleteMarker, btnReset, btnDone;
    EditText edtMarker;
    SeekBar skBarRadius;
	
//    /**
//     * Populate the activity with the top-level headers.
//     */
//    @Override
//    public void onBuildHeaders(List<Header> target) {
////        loadHeadersFromResource(R.xml.preference_headers, target);
//        setContentView(R.layout.activity_active_locations);
//    }
    
    /**
     * Initializes the map and zooms in on the current location
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_locations);
        
        
        // get the map fragment
        mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        
        // initialize the Map Manager
        mapManager = new MapManager(mMap);
        
        // initialize the location storage
        ls = new LocationsStorage(this);
        ArrayList<VecinityMarker> loadedMarkers = ls.load(mMap);
        mapManager.setAllSavedMarkers(loadedMarkers);
        
        
        // if the current location is known, show it
        if (Locality.getInstance(this).getLastLocation() != null) {
        	
        	mapManager.setCurrentLocation(Locality.getInstance(this));
        	mapManager.zoomOnCurrentLocation();
        }
        
        
        // initializing the UI variables from the layout
        laySaveMarker = (RelativeLayout) findViewById(R.id.laySaveMarker);
        btnSaveMarker = (Button) findViewById(R.id.btnSaveMarker);
        btnDeleteMarker = (Button) findViewById(R.id.btnDeleteMarker);
        btnReset = (Button) findViewById(R.id.btnReset);
        btnDone = (Button) findViewById(R.id.btnDone);
        edtMarker = (EditText) findViewById(R.id.edtMarkerName);
        skBarRadius = (SeekBar) findViewById(R.id.skBarRadius);

        
        // setting up action when the user taps the map => create new marker
        mMap.setOnMapClickListener(new OnMapClickListener() {
			@Override
			public void onMapClick(LatLng point) {

				
				// get rid of any unsaved markers
				mapManager.cleanCurrentTemporaryUnsavedMarker();

				// add a new marker
				mapManager.addTemporaryMarker(point);
				

				// ask for the marker name by prompting the user
				showEmptyPrompt(mapManager.getCurrentMarker());
				
			} 
		});
        
        
        // setting up action when the user taps an existing marker => show marker info
		mMap.setOnMarkerClickListener(new OnMarkerClickListener() {
			
			@Override
			public boolean onMarkerClick(Marker marker) {

				// if an unsaved marker was present on the map, remove it
				mapManager.cleanCurrentTemporaryUnsavedMarker();
				
				// change the current marker 
				mapManager.setTemporaryMarker(mapManager.getVecinityMarkerByMarker(marker));
				
				
				return false;
			}
		});
		
		
		/**
		 * Edit a marker when its info window is tapped
		 */
		mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
			
			@Override
			public void onInfoWindowClick(Marker marker) {
				
				// looks for the marker in the list of saved markers
				VecinityMarker vm = mapManager.getVecinityMarkerByMarker(marker);
				
				// show edit prompt only for the saved markers
				if (vm != null)
					showEditPrompt(vm);
				
				// do nothing for an unsaved marker
				
				
			}
		});
		
		
		/**
		 * Show the keyboard when the user focuses on the text box
		 * Hide the keyboard when the focus is lost
		 */
		edtMarker.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				
				if (hasFocus) 
					imm.showSoftInput(edtMarker, InputMethodManager.SHOW_IMPLICIT);
				else 
					imm.hideSoftInputFromWindow(edtMarker.getWindowToken(), 0);

				if(edtMarker.getText().toString().equals(""))
					btnSaveMarker.setEnabled(false);
				else
					btnSaveMarker.setEnabled(true);
				
			}
		});

		
		/**
		 * Disable the "Save" button if name is empty 
		 * Enable the "Save" button if the name is not empty
		 */
		edtMarker.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

				
				if(edtMarker.getText().toString().equals("")) {
					btnSaveMarker.setEnabled(false);
					mapManager.setTemporaryMarkerTitle(" ");
					mapManager.showTemporaryMarkerInfoWindow();	// updates the title on the map
				} else {
					btnSaveMarker.setEnabled(true);
					mapManager.setTemporaryMarkerTitle(edtMarker.getText().toString());
					mapManager.showTemporaryMarkerInfoWindow();// updates the title on the map
//					mapManager.saveTemporaryMarker();
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// do nothing
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// do nothing
				
			}
		});
		
		
		// set the action on the reset button
		btnReset.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mapManager.reset();
				mapManager.zoomOnCurrentLocation();
				ls.save(mapManager.getAllSavedMarkers());
			}
			
		});
		
		
		// implement the action when the button Done is pressed
		final Activity a = this;
		btnDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				a.onBackPressed();
			}
			
		});
		
    }
    
    /**
     * Shows the prompt for any marker, by centering the map
     * with the marker in the middle
     * @param m
     */
    private void showPrompt (VecinityMarker m) {
		laySaveMarker.setVisibility(laySaveMarker.VISIBLE);
		skBarRadius.setProgress(getBarValueFromRadius(m.getRadius()));
		mMap.animateCamera(CameraUpdateFactory.newLatLng(m.getPosition()));
		implementOnClickDeleteButton(m);
		implementOnClickSaveButtion(m);
		implementOnChangeSeekBarValue(m);
		
    }
    
    
    /**
     * Show the empty prompt (for the new markers)
     * @param m
     */
    private void showEmptyPrompt(VecinityMarker m) {
		edtMarker.setText("");
    	showPrompt(m);
    }
    
    
    /**
     * Show the edit prompt (for the saved markers)
     * @param m
     */
    private void showEditPrompt(VecinityMarker m) {
		edtMarker.setText(m.getTitle());
    	showPrompt(m);
    }
    
    /**
     * Hide the edit prompt for any marker
     */
    private void hideEditPrompt() {
    	
    	// hide the edit prompt
    	laySaveMarker.setVisibility(laySaveMarker.INVISIBLE);
    	
    	// hide keyboard
    	edtMarker.clearFocus();
    }

    
    /**
     * Implements action when the delete button is pressed
     * The marker is deleted
     * @param m
     */
    private void implementOnClickDeleteButton (final VecinityMarker m) {
    	// canceling entering a marker's name
		btnDeleteMarker.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				// delete the marker if it was never saved
				mapManager.removeCurrentMarker();

				// saving the new marker on the device
				ls.save(mapManager.getAllSavedMarkers());
				
				// hides the prompt
				hideEditPrompt();
			}
		});
    }
    
    
    /**
     * Implements action when the save button is pressed
     * Saves the temporary marker
     * @param m
     */
    private void implementOnClickSaveButtion (final VecinityMarker m) {
    	btnSaveMarker.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				// set the new marker title
				mapManager.setTemporaryMarkerTitle(edtMarker.getText().toString());
				
				// set the new radius based on the seekBar
				mapManager.setTemporaryMarkerRadius(getRadiusFromBarValue(skBarRadius.getProgress()));
				
				// add the location to the list
				mapManager.saveTemporaryMarker();
				
				// redisplay the marker title
				mapManager.showTemporaryMarkerInfoWindow();
				
				// saving the new marker on the device
				ls.save(mapManager.getAllSavedMarkers());
				
				// hide the prompt
				hideEditPrompt();
			}
		});
    }
    
    
    /**
     * Update the new radius on the map automatically
     * @param m
     */
    private void implementOnChangeSeekBarValue (final VecinityMarker m) {
    	skBarRadius.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar
					) {
				// do nothing
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// do nothing
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				
				// updated the radius of the marker
				mapManager.setTemporaryMarkerRadius(getRadiusFromBarValue(skBarRadius.getProgress()));
				
				// redraws the circle around the marker
				mapManager.redrawTemporaryMarkerRadius();
			}
		});
    }

    
    /**
     * Using the formula 2^(val/10), calculates the radius in meters of the area around the marker
     * @param val
     * @return
     */
    private double getRadiusFromBarValue(int val) {
    	return Math.pow(2.0, val/10.0);
    }
    
    
    /**
     * Using the formula 10 * log2(val), calculated the position of the cursor on the seekBar
     * @param val
     * @return
     */
    private int getBarValueFromRadius(double val) {
    	return (int) (10*Math.log(val)/Math.log(2));
    }
    
}
