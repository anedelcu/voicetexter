package com.alexnedelcu.voicetexter.map.ui;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;

public class LocationsStorage {
	private ArrayList<Double> latitudes;
	private ArrayList<Double> longitudes;
	private ArrayList<Double> radi;
	private ArrayList<String> titles;

	private final static String latEntryKey = "markerlat.d";
	private final static String longEntryKey = "markerlong.d";
	private final static String radiEntryKey = "markerradi.d";
	private final static String titleEntryKey = "markertitles.d";
	
	private Activity activity;
	
	public LocationsStorage(Activity activity) {
		this.activity = activity;
	}
	
	
	public void saveToFile (String key, Object data) {
		
		SharedPreferences mPrefs=activity.getSharedPreferences(activity.getApplicationInfo().name, Context.MODE_PRIVATE);
		SharedPreferences.Editor ed=mPrefs.edit();
		Gson gson = new Gson(); 
		ed.putString(key, gson.toJson(data));
		ed.commit();
		
//		FileOutputStream fos;
//		try {
//			fos = activity.getApplicationContext().openFileOutput(fileName, Context.MODE_PRIVATE);
//			ObjectOutputStream os = new ObjectOutputStream(fos);
//			os.writeObject(data);
//			os.close();
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			
//			
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			
//			
//			
//			e.printStackTrace();
//		}
	}
	
	public Object loadFromFile (String key, Class c) {
		SharedPreferences mPrefs=activity.getSharedPreferences(activity.getApplicationInfo().name, Context.MODE_PRIVATE);
		String objJson = mPrefs.getString(key, null);
		
		Gson gson = new Gson();
//		JsonParser parser=new JsonParser();
		return gson.fromJson(objJson, c);
		
//		FileInputStream fis;
//		try {
//			fis = activity.getApplicationContext().openFileInput(fileName);
//			ObjectInputStream is = new ObjectInputStream(fis);
//			Object result = is.readObject();
//			is.close();
//			return result;
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
//		return null;
	}
	
	public void save(ArrayList<VecinityMarker> markers) {
		latitudes = new ArrayList<Double>();
		longitudes = new ArrayList<Double>();
		radi = new ArrayList<Double>();
		titles = new ArrayList<String>();
		
		for (int i=0; i<markers.size(); i++) {
			latitudes.add(markers.get(i).getLatitude());
			longitudes.add(markers.get(i).getLongitude());
			radi.add(markers.get(i).getRadius());
			titles.add(markers.get(i).getTitle());
		}
		
		saveToFile(latEntryKey, latitudes);
		saveToFile(longEntryKey, longitudes);
		saveToFile(radiEntryKey, radi);
		saveToFile(titleEntryKey, titles);
	}
	
	public ArrayList<VecinityMarker> load (GoogleMap map) {
		ArrayList<VecinityMarker> markers = null;
		markers = new ArrayList<VecinityMarker>();
		

		latitudes = (ArrayList<Double>) loadFromFile(latEntryKey, ArrayList.class);
		longitudes = (ArrayList<Double>) loadFromFile(longEntryKey, ArrayList.class);
		radi = (ArrayList<Double>) loadFromFile(radiEntryKey, ArrayList.class);
		titles = (ArrayList<String>) loadFromFile(titleEntryKey, ArrayList.class);

		System.out.println(latitudes);
		System.out.println(longitudes);
		System.out.println(radi);
		System.out.println(titles);
		
		if (latitudes != null) {
			for (int i=0; i<latitudes.size(); i++) {
				markers.add(new VecinityMarker(
						map,
						new LatLng(latitudes.get(i), longitudes.get(i)),
						radi.get(i)
						));
				markers.get(i).addOnMap();
				markers.get(i).addRadiusArea();
				markers.get(i).getMarker().setTitle(titles.get(i));
			}
		}
		
		return markers;
	}
}
