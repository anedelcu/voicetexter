package com.alexnedelcu.voicetexter.message.listeners;

import java.util.ArrayList;

import com.alexnedelcu.voicetexter.util.Command;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.annotation.TargetApi;
import android.os.Build;
import android.view.accessibility.AccessibilityEvent;

public class ThirdPartyAppListener extends AccessibilityService  {
	private static ArrayList<String> packages = new ArrayList<String>();
	private static ArrayList<Command> listenerActions = new ArrayList<Command>();

	public void onAccessibilityEvent(AccessibilityEvent event) {
		System.out.println("Event occured");
		for (int i=0; i<packages.size(); i++) {
			System.out.println("Checking "+packages.get(i));
			if (packages.get(i).equals(event.getPackageName())){
				System.out.println("Executing "+packages.get(i));
				listenerActions.get(i).execute(event);
			}
			
		}
	}
	
	public static void addThirdPartyListener(String packageName, Command c) {
		packages.add(packageName);
		listenerActions.add(c);
	}
	
	@Override
	public void onServiceConnected() {
		
	    AccessibilityServiceInfo info = new AccessibilityServiceInfo();
	    
	    // Set the type of events that this service wants to listen to.  Others
	    // won't be passed to this service.
	    info.eventTypes = AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED;
	    
	    System.out.println("ONSERVICECONNECTED");

	    // If you only want this service to work with specific applications, set their
	    // package names here.  Otherwise, when the service is activated, it will listen
	    // to events from all applications.
//	    info.packageNames = new String[]
//	            {"com.google.android.gm", "com.facebook.katana"};

	    // Set the type of feedback your service will provide.
	    info.feedbackType = AccessibilityServiceInfo.FEEDBACK_ALL_MASK;

	    // Default services are invoked only if no package-specific ones are present
	    // for the type of AccessibilityEvent generated.  This service *is*
	    // application-specific, so the flag isn't necessary.  If this was a
	    // general-purpose service, it would be worth considering setting the
	    // DEFAULT flag.

	    // info.flags = AccessibilityServiceInfo.DEFAULT;

	    info.notificationTimeout = 100;

	    this.setServiceInfo(info); 

	}

	@Override
	public void onInterrupt() {
		// TODO Auto-generated method stub
	    System.out.println("onInterrupt");
	}

}
