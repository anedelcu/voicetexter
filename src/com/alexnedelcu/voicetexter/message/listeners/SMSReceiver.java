package com.alexnedelcu.voicetexter.message.listeners;

import com.alexnedelcu.voicetexter.IO.TextToVoice;
import com.alexnedelcu.voicetexter.datatypes.Message;
import com.alexnedelcu.voicetexter.util.Command;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

public class SMSReceiver extends BroadcastReceiver {

    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
//    private static TextToVoice ttv;
    private static Command command;

    @Override
    public void onReceive(Context context, Intent intent) {
//    	ttv = new TextToVoice(context);
        if (intent.getAction().equals(SMS_RECEIVED)) {
            Bundle bundle = intent.getExtras();

            
            if (bundle != null) {            	
        		
                // get sms objects
                Object[] pdus = (Object[]) bundle.get("pdus");
                if (pdus.length == 0) {
                    return;
                }
                // large message might be broken into many
                SmsMessage[] messages = new SmsMessage[pdus.length];
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < pdus.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    sb.append(messages[i].getMessageBody());
                }
                String sender = messages[0].getOriginatingAddress();
                String message = sb.toString();
                
                // build the message object
                final Message m = new Message();
                m.setFrom(sender);
                m.setBodyReceived(message);
            	
            	command.execute(m);
            }
        }
    }

	public static void setActionOnReceive(Command pCommand) {
		command = pCommand;
	}

}