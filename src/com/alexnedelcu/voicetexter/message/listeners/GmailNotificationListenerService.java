package com.alexnedelcu.voicetexter.message.listeners;

import com.alexnedelcu.voicetexter.IO.IO;
import com.alexnedelcu.voicetexter.message.listeners.adaptees.NotificationReadText;
import com.alexnedelcu.voicetexter.util.Command;

import android.annotation.TargetApi;
import android.app.Notification;
import android.os.Build;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityEvent;


public class GmailNotificationListenerService extends ThirdPartyAppListener {

	public static void register() {
		ThirdPartyAppListener.addThirdPartyListener("com.google.android.gm", new Command() {

			@Override
			public void execute(Object pEvent) {
				AccessibilityEvent event = (AccessibilityEvent) pEvent;

				// get the notification 
				Notification n = (Notification) event.getParcelableData();

				String email = TextUtils.join(" ", NotificationReadText.getText(n));
				
				// silly way to check if this is an email
				if (email.contains("@")) {
					IO.getInstance().output("New email received!");
				}
			}
		});
	}

}
