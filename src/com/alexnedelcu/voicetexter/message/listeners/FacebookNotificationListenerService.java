package com.alexnedelcu.voicetexter.message.listeners;

import java.util.List;

import com.alexnedelcu.voicetexter.IO.IO;
import com.alexnedelcu.voicetexter.message.listeners.adaptees.NotificationReadText;
import com.alexnedelcu.voicetexter.util.Command;

import android.app.Notification;
import android.text.TextUtils;
import android.view.accessibility.AccessibilityEvent;


public class FacebookNotificationListenerService extends ThirdPartyAppListener {

	public static void register() {
		ThirdPartyAppListener.addThirdPartyListener("com.facebook.orca", new Command() {

			@Override
			public void execute(Object pEvent) {
				AccessibilityEvent event = (AccessibilityEvent) pEvent;

				List<String> contentData = NotificationReadText.getText((Notification) event.getParcelableData());
				
//				DEBUG CODE IN CASE FACEBOOK NOTIFYING SERVICE STOPS WORKING
//				for (int i = 0; i< contentData.size(); i++)
//				    System.out.println(i + ": " + contentData.get(i));
//				System.out.println("FB: " + event.getPackageName());
//				System.out.println("FBContent: " + NotificationReadText.getText((Notification) event.getParcelableData()));
				
				if (!((String)contentData.get(0)).equals("Chat heads active")) {
					IO.getInstance().output("New Facebook message!!!");
				}
			}
		});
	}
}
