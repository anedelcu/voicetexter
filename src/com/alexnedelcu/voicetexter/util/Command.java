package com.alexnedelcu.voicetexter.util;

public interface Command {
	public void execute(Object params);
}
