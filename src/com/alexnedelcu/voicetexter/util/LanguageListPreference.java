package com.alexnedelcu.voicetexter.util;

import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.preference.ListPreference;
import android.util.AttributeSet;

public class LanguageListPreference extends ListPreference {

	public LanguageListPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		ArrayList<String> entries = new ArrayList<String>();
		ArrayList<String> entries_values = new ArrayList<String>();
		String previous = "";
		
		for(int i=0; i<Locale.getAvailableLocales().length; i++) {
			if (!previous.equals(Locale.getAvailableLocales()[i].getDisplayLanguage())) {
				entries.add(Locale.getAvailableLocales()[i].getDisplayLanguage());
				entries_values.add(Locale.getAvailableLocales()[i].getISO3Language());
				previous = Locale.getAvailableLocales()[i].getDisplayLanguage();
			}
		}

		// converting to the CharSequence [] format
		CharSequence[] entries_cs = entries.toArray(new CharSequence[entries.size()]);
		CharSequence[] entries_values_cs = entries.toArray(new CharSequence[entries.size()]);

		System.out.println(entries_cs.length);
		System.out.println(entries_values_cs.length);
		
        setEntries(entries_cs);
        setEntryValues(entries_values_cs);
		
	}
	
	public LanguageListPreference(Context context) {
		super(context, null);
	}
	

}
