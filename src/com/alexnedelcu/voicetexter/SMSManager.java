package com.alexnedelcu.voicetexter;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;

import com.alexnedelcu.voicetexter.IO.IO;
import com.alexnedelcu.voicetexter.commands.ReadSMSCommand;
import com.alexnedelcu.voicetexter.message.listeners.FacebookNotificationListenerService;
import com.alexnedelcu.voicetexter.message.listeners.GmailNotificationListenerService;
import com.alexnedelcu.voicetexter.message.listeners.SMSReceiver;
import com.alexnedelcu.voicetexter.states.Audio;
import com.alexnedelcu.voicetexter.states.Motion;
import com.alexnedelcu.voicetexter.util.Command;
import com.facebook.widget.FacebookDialog;


public class SMSManager {
	private static SMSManager instance = null;
	private IO IO;
	private SMSReceiver smsReceiver;
	private PreferenceInterpreter settings;
	private static Activity activity;
	
	private static Command actionWhenActivate = null;
	private static Command actionWhenDeactivate = null;

	private static Audio audio;
//	private static Motion motion;
	private static Locality locality;

	// using Singleton pattern for instantiation
	public static SMSManager getInstance () {
		return instance;
	}
	
	// using Singleton pattern for instantiation
	public static SMSManager getInstance (Activity a) {
		activity = a;
		instance = new SMSManager(activity.getApplicationContext());
		audio = new Audio(activity.getApplicationContext());
		locality = Locality.getInstance(activity);
//		if (audio.isHeadsetOn())
//			System.out.println("HEADSPHONES ARE ON!!!!!!!!!!!!!!!!!!!!");
		
//		motion.getSpeed(context);

		
		
		
		return instance;
	}
	
	public SMSManager(final Context c) {
		IO = new IO(c);
		settings = new PreferenceInterpreter(c);
		SMSReceiver.setActionOnReceive(new Command () {

			@Override
			public void execute(Object params) {
				boolean allowToRead = false;
				if (settings.isDockedModeAllowed() && audio.isHeadsetOn())
					allowToRead = true;
				
				if (settings.isDriveModeAllowed()) // to be completed
					allowToRead = true;
				
				if (settings.isSpecificLocationsAllowed()) // to be completed
					allowToRead = true;
				
				if (allowToRead)
					(new ReadSMSCommand()).execute(params);
			}
			
		});

//		FacebookNotificationListenerService.register();
//		GmailNotificationListenerService.register();
		
	}

	public void setActionOnDrivingModeActivation(Command command) {
		locality.setActionOnDrivingModeActivation(command);
	}

	public void setActionOnDrivingModeDeactivation(Command command) {
		locality.setActionOnDrivingModeDeactivation(command);
	}
	
}
