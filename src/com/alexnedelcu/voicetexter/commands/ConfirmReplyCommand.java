package com.alexnedelcu.voicetexter.commands;

import android.telephony.gsm.SmsManager;

import com.alexnedelcu.voicetexter.IO.IO;
import com.alexnedelcu.voicetexter.datatypes.Message;
import com.alexnedelcu.voicetexter.util.Command;

public class ConfirmReplyCommand implements Command {
	IO io;
	Message m;
	
	public ConfirmReplyCommand (Message m) {
		io = IO.getInstance();
		this.m = m;
	}
	
	@Override
	public void execute(final Object pMessage) {
		final String reply = (String)pMessage;
		
		io.getYesNoAnswer("Your message is: " + reply + ". Is that correct?", new Command() {

			@Override
			public void execute(Object params) {
				m.setBodyReplied(reply);
				m.send();
				io.output("Message being sent!");
			}
			
		}, new ReplyInputCommand(m));
	}

}
