package com.alexnedelcu.voicetexter.commands;

import java.util.ArrayList;

import android.os.Bundle;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.widget.Toast;

import com.alexnedelcu.voicetexter.IO.IO;
import com.alexnedelcu.voicetexter.datatypes.Message;
import com.alexnedelcu.voicetexter.util.Command;

public class ReadSMSCommand implements Command {
	IO io;
	
	public ReadSMSCommand () {
		io = IO.getInstance();
	}
	
	@Override
	public void execute(Object param) {
		// TODO Auto-generated method stub
		final Message m = (Message) param;

        io.output("New text message!");
        
        io.getYesNoAnswer("Would you like me to read it?", new Command() { // YES

			@Override
			public void execute(Object params) {
				// this should become a contact lookup
				ArrayList<String> digits = new ArrayList<String>();
				for (int i=0; i<m.getFrom().length(); i++) 
					digits.add(""+m.getFrom().charAt(i));
				
				io.output("Message from "+TextUtils.join((CharSequence)" ", digits));
            	io.output(m.getBodyReceived());
            	

                // reply to the text
                (new ReplyCommand(m)).execute(params);
                
            	
			}
        	
        }, new Command() {	// NO

			@Override
			public void execute(Object params) {
				// TODO Auto-generated method stub
				io.output("Gotcha!");
			}
        	
        });
	}

}
