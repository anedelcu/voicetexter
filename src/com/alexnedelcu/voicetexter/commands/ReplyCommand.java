package com.alexnedelcu.voicetexter.commands;

import com.alexnedelcu.voicetexter.IO.IO;
import com.alexnedelcu.voicetexter.datatypes.Message;
import com.alexnedelcu.voicetexter.util.Command;

public class ReplyCommand implements Command {
	IO io ;
	Message m;
	
	public ReplyCommand(Message m) {
		io = IO.getInstance();
		this.m = m;
	}
	
	@Override
	public void execute(Object params) {
		
		
		io.getYesNoAnswer("Would you like to reply to it?", new ReplyInputCommand(m),
			new Command() {	// NO
			
				@Override
				public void execute(Object params) {
					io.output("Okay!");
				}
        	
        });
		
	}
}
