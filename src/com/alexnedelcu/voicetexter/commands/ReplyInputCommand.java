package com.alexnedelcu.voicetexter.commands;

import com.alexnedelcu.voicetexter.IO.IO;
import com.alexnedelcu.voicetexter.datatypes.Message;
import com.alexnedelcu.voicetexter.util.Command;

public class ReplyInputCommand implements Command {
	IO io;
	Message m;
	
	public ReplyInputCommand (Message m) {
		io = IO.getInstance();
		this.m = m;
	}

	@Override
	public void execute(Object params) {
		// send message
    	io.inputMessage("Please leave your message after the tone!", new ConfirmReplyCommand(m));
	}

}
